package org.alcibiade.supertape.android

interface LogFlow {
    fun logMessage(message: String)
}
