package org.alcibiade.supertape.android

import android.Manifest
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.RecyclerView
import android.view.View
import kotlinx.android.synthetic.main.activity_tapes.*
import org.alcibiade.supertape.android.audio.input.AudioListener
import org.alcibiade.supertape.android.fragment.MyTapeRecyclerViewAdapter
import org.alcibiade.supertape.android.fragment.TapeFragment
import org.alcibiade.supertape.android.fragment.dummy.DummyContent

private const val REQUEST_RECORD_AUDIO_PERMISSION = 200

class TapesActivity : AppCompatActivity(), LogFlow, TapeFragment.OnListFragmentInteractionListener {

    private lateinit var recyclerView: RecyclerView
    private lateinit var viewAdapter: RecyclerView.Adapter<*>

    private var audioListener = AudioListener(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tapes)

        viewAdapter = MyTapeRecyclerViewAdapter(DummyContent.ITEMS, this)
        recyclerView = findViewById<RecyclerView>(R.id.list).apply {
            adapter = viewAdapter
        }

        ActivityCompat.requestPermissions(this,
            arrayOf(Manifest.permission.RECORD_AUDIO),
            REQUEST_RECORD_AUDIO_PERMISSION)

        // Example of a call to a native method
        log_text.text = stringFromJNI() + "\n"
    }

    override fun onDestroy() {
        super.onDestroy()
        audioListener.stop()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == REQUEST_RECORD_AUDIO_PERMISSION) {
            audioListener.start()
        }
    }

    override fun logMessage(message: String) {
        this.runOnUiThread {
            log_text.append("$message\n")
            scroll_text.fullScroll(View.FOCUS_DOWN)
        }
    }

    override fun onListFragmentInteraction(item: DummyContent.DummyItem?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    /**
     * A native method that is implemented by the 'native-lib' native library,
     * which is packaged with this application.
     */
    external fun stringFromJNI(): String

    companion object {

        // Used to load the 'native-lib' library on application startup.
        init {
            System.loadLibrary("native-lib")
        }
    }
}
