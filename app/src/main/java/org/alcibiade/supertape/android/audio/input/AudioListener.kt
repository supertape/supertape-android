package org.alcibiade.supertape.android.audio.input

import android.media.AudioFormat
import android.media.AudioRecord
import android.media.MediaRecorder
import android.os.Process
import android.util.Log
import org.alcibiade.supertape.android.LogFlow


const val LOG_TAG = "AudioListener"
const val SAMPLE_RATE = 44100

class AudioListener(private var logFlow: LogFlow) {
    private var shouldContinue = true

    fun start() {
        recordAudio()
        Log.d(LOG_TAG, "Audio listener started")
    }

    /**
     * Stops a recording that has been previously started.
     */
    fun stop() {
        this.shouldContinue = false
        Log.d(LOG_TAG, "Audio listener stopped")
    }

    fun recordAudio() {
        Thread(Runnable {
            Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_AUDIO)

            // buffer size in bytes
            var bufferSize = AudioRecord.getMinBufferSize(
                SAMPLE_RATE,
                AudioFormat.CHANNEL_IN_MONO,
                AudioFormat.ENCODING_PCM_16BIT)

            if (bufferSize == AudioRecord.ERROR || bufferSize == AudioRecord.ERROR_BAD_VALUE) {
                bufferSize = SAMPLE_RATE * 2
            }

            bufferSize *= 10

            val audioBuffer = ShortArray(bufferSize / 2)

            val record = AudioRecord(
                MediaRecorder.AudioSource.DEFAULT,
                SAMPLE_RATE,
                AudioFormat.CHANNEL_IN_MONO,
                AudioFormat.ENCODING_PCM_16BIT,
                bufferSize)

            if (record.state != AudioRecord.STATE_INITIALIZED) {
                Log.e(LOG_TAG, "Audio Record can't initialize!")
                return@Runnable
            }

            record.startRecording()

            Log.d(LOG_TAG, "Start recording")

            var shortsRead: Long = 0
            while (shouldContinue) {
                val numberOfShort = record.read(audioBuffer, 0, audioBuffer.size)
                shortsRead += numberOfShort.toLong()

                this.logFlow.logMessage("Processing block of $numberOfShort samples")
            }

            record.stop()
            record.release()

            Log.d(LOG_TAG, "Recording stopped. Samples read: $shortsRead")
        }).start()
    }
}
